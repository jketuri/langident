#!/usr/bin/env python
# coding: utf-8

from collections import Counter
from itertools import chain
from os import listdir
from os.path import isfile, join
import re
from sys import argv
from typing import Dict, List, Set, Tuple
import unicodedata
import xml.etree.ElementTree as ET
from joblib import dump, load
import matplotlib.pyplot as plt
from pycountry import languages
import pycrfsuite
from scipy.stats import expon
from sklearn_crfsuite import CRF, metrics
from sklearn.metrics import classification_report, make_scorer
from sklearn_crfsuite.metrics import flat_classification_report, flat_f1_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import LabelBinarizer


C1 = 0.8209819861677927
C2 = 0.03494571973715279
C2_2 = 0.16561595542996918


def _word2features(
        sentence: str,
        index: int
):
    word = sentence[index]
    features = [
        'bias',
        'word.lower=' + word.lower(),
        'word[-3:]=' + word[-3:],
        'word[-2:]=' + word[-2:],
        'word[:2]=' + word[:2],
        'word[:3]=' + word[:3],
        'word.isdigit=%s' % word.isdigit(),
        'word.istitle=%s' % word.istitle(),
        'word.isupper=%s' % word.isupper(),
        'word.length=%s' % len(word)
    ]
    if index > 0:
        word1 = sentence[index - 1]
        features.extend([
            '-1:word.lower=' + word1.lower(),
            '-1:word[-3:]=' + word1[-3:],
            '-1:word[-2:]=' + word1[-2:],
            '-1:word[:2]=' + word1[:2],
            '-1:word[:3]=' + word1[:3],
            '-1:word.isdigit=%s' % word1.isdigit(),
            '-1:word.istitle=%s' % word1.istitle(),
            '-1:word.isupper=%s' % word1.isupper(),
            '-1:word.length=%s' % len(word1)
        ])
    else:
        features.append('BOS')
    if index < len(sentence) - 1:
        word1 = sentence[index + 1]
        features.extend([
            '+1:word.lower=' + word1.lower(),
            '+1:word[-3:]=' + word1[-3:],
            '+1:word[-2:]=' + word1[-2:],
            '+1:word[:2]=' + word1[:2],
            '+1:word[:3]=' + word1[:3],
            '+1:word.isdigit=%s' % word1.isdigit(),
            '+1:word.istitle=%s' % word1.istitle(),
            '+1:word.isupper=%s' % word1.isupper(),
            '+1:word.length=%s' % len(word1)
        ])
    else:
        features.append('EOS')
    return features


def _sentence2features(
        sentence: str,
        use_ULI2020: bool = False
) -> List[List[str]]:
    tokens = _sentence2tokens(sentence, use_ULI2020)
    return [
        _word2features(tokens, index) for index in range(len(tokens))]


def strip_sentence(
        sentence: str
) -> str:
    index = sentence.find(' ')
    if sentence[:index].strip().isdigit():
        sentence = sentence[index + 1:]
    index = sentence.find(' :: ')
    if index != -1:
        sentence = sentence[:index]
    else:
        index = sentence.find('://')
        if index != -1:
            sentence = sentence[:sentence.rfind(' ', 0, index) + 1]
    return sentence


def _sentence2tokens(
        sentence: str,
        use_ULI2020: bool = False
) -> List[List[str]]:
    if use_ULI2020:
        sentence = strip_sentence(sentence)
    cat = None
    tokens = []
    token = ''
    for c in sentence:
        cat1 = unicodedata.category(c)[0]
        if cat1 != cat:
            if token and cat == 'L':
                tokens.append(token)
            token = ''
        token += c
        cat = cat1
    return tokens


def _read_TED_language_codes() -> Dict[str, str]:
    print('_read_TED_language_codes')
    language_ids = {}
    with open(
            'TED-Multilingual-Parallel-Corpus/Language_codes.txt',
            encoding='utf-8') as language_codes_file:
        for line in language_codes_file:
            line = line.strip()
            blank = line.find(' ')
            language_ids[line[blank + 1:].strip().replace(' (', '_(')] = line[
                :blank].split('-')[0]
    return language_ids


def _read_TED(
        x_train: List[List[str]],
        y_train: List[List[str]],
        test_count: int = 0
) -> Set[str]:
    print('_read_TED')
    language_ids = set()
    language_codes = _read_TED_language_codes()
    lang_folder = 'TED-Multilingual-Parallel-Corpus/Monolingual_data'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if isfile(lang_file_path):
            if not lang_file_name.endswith('.txt'):
                continue
            language_id = language_codes[lang_file_name.replace('.txt', '')]
            language_ids.add(language_id)
            with open(lang_file_path, 'rt', encoding='utf-8') as lang_file:
                count = 0
                for line in lang_file:
                    count += 1
                    if count < test_count:
                        continue
                    sentence_features = _sentence2features(line)
                    if sentence_features:
                        x_train.append(sentence_features)
                        y_train.append([language_id] * len(sentence_features))
    return language_ids


def _read_bible_language_ids() -> Set[str]:
    print('_read_bible_language_ids')
    language_ids = set()
    lang_folder = 'bible-corpus/bibles'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if isfile(lang_file_path):
            root = ET.fromstring(open(
                lang_file_path, encoding='utf-8').read())
            language_id = next(root.iter('language')).get('id')
            language_ids.add(language_id)
    return language_ids


def _read_bibles(
        x_train: List[List[str]],
        y_train: List[List[str]]
) -> Set[str]:
    print('_read_bibles')
    language_ids = set()
    lang_folder = 'bible-corpus/bibles'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if isfile(lang_file_path):
            root = ET.fromstring(open(
                lang_file_path, encoding='utf-8').read())
            language_id = next(root.iter('language')).get('id')
            language_ids.add(language_id)
            for segment in root.iter('seg'):
                if segment.text:
                    sentence_features = _sentence2features(segment.text)
                    if sentence_features:
                        x_train.append(sentence_features)
                        y_train.append([language_id] * len(sentence_features))
    return language_ids


def _read_ULI2020_language_ids() -> Set[str]:
    print('_read_ULI2020_language_ids')
    language_ids = set()
    lang_folder = 'ULI2020_training'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if isfile(lang_file_path):
            if not lang_file_name.endswith('.txt'):
                continue
            language_id = lang_file_name[:-len('.txt')].split('_')[0]
            if not language_id.isupper():
                language_ids.add(language_id)
    return language_ids


def _read_ULI2020(
        x_train: List[List[str]],
        y_train: List[List[str]],
        max_count: int = 10000,
        test_count: int = 0
) -> Set[str]:
    print('_read_ULI2020')
    language_ids = set()
    lang_folder = 'ULI2020_training'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if isfile(lang_file_path):
            if not lang_file_name.endswith('.txt'):
                continue
            language_id = lang_file_name[:-len('.txt')].split('_')[0]
            if language_id.isupper():
                continue
            language_ids.add(language_id)
            print('Reading', lang_file_path, 'for language', language_id)
            with open(lang_file_path, 'rt', encoding='utf-8') as lang_file:
                count = 0
                for line in lang_file:
                    count += 1
                    if count < test_count:
                        continue
                    if count > max_count:
                        break
                    sentence_features = _sentence2features(
                        line, use_ULI2020=True)
                    if sentence_features:
                        x_train.append(sentence_features)
                        y_train.append([language_id] * len(sentence_features))
    return language_ids


def _optimize(
        x_train: List[List[str]],
        y_train: List[List[str]],
        labels: List[str],
        use_sklearn: bool = False,
        algorithm: str = 'lbfgs'
) -> object:
    print('_optimize')
    # define fixed parameters and parameters to search
    crf = CRF(
        algorithm=algorithm,
        max_iterations=100,
        all_possible_transitions=True
    )
    if algorithm == 'l2sgd':
        params_space = {
            'c2': expon(scale=0.05),
        }
    else:
        params_space = {
            'c1': expon(scale=0.5),
            'c2': expon(scale=0.05),
        }
    # use the same metric for evaluation
    f1_scorer = make_scorer(
        metrics.flat_f1_score,
        average='weighted', labels=labels)
    # search
    rs = RandomizedSearchCV(
        crf, params_space,
        cv=3,
        verbose=1,
        n_jobs=-1,
        n_iter=50,
        scoring=f1_scorer)
    rs.fit(x_train, y_train)
    print('best params:', rs.best_params_)
    print('best CV score:', rs.best_score_)
    if use_sklearn:
        print('model size: {:0.2f}M'.format(
            rs.best_estimator_.size_ / 1000000))
    return rs


def _check_parameter_space(
        rs: object
):
    print('_check_parameter_space')
    _x = [s.parameters['c1'] for s in rs.grid_scores_]
    _y = [s.parameters['c2'] for s in rs.grid_scores_]
    _c = [s.mean_validation_score for s in rs.grid_scores_]
    fig = plt.figure()
    fig.set_size_inches(12, 12)
    ax = plt.gca()
    ax.set_yscale('log')
    ax.set_xscale('log')
    ax.set_xlabel('C1')
    ax.set_ylabel('C2')
    ax.set_title(
        'Randomized Hyperparameter Search CV Results '
        '(min={:0.3}, max={:0.3})'.format(min(_c), max(_c)))
    ax.scatter(_x, _y, c=_c, s=60, alpha=0.9, edgecolors=[0, 0, 0])
    plt.savefig('parameter_space')
    plt.close()
    print('Dark blue => {:0.4}, dark red => {:0.4}'.format(min(_c), max(_c)))


def _make_crf(
        algorithm: str
):
    if algorithm == 'l2sgd':
        return CRF(
            algorithm=algorithm,
            max_iterations=100,      # stop earlier
            # include transitions that are possible, but not observed
            all_possible_transitions=True
        )
    if algorithm == 'lbfgs':
        return CRF(
            algorithm=algorithm,
            c1=C1,                   # coefficient for L1 penalty
            c2=C2,                   # coefficient for L2 penalty
            max_iterations=100,      # stop earlier
            # include transitions that are possible, but not observed
            all_possible_transitions=True
        )
    return CRF(
        algorithm=algorithm,
        max_iterations=100,          # stop earlier
        # include transitions that are possible, but not observed
        all_possible_transitions=True
    )


def _set_training(
        trainer: pycrfsuite.Trainer,
        algorithm: str
):
    if algorithm == 'l2sgd':
        trainer.set_params({
            'max_iterations': 100,   # stop earlier
            # include transitions that are possible, but not observed
            'feature.possible_transitions': True})
    elif algorithm == 'lbfgs':
        trainer.set_params({
            'c1': C1,                # coefficient for L1 penalty
            'c2': C2,                # coefficient for L2 penalty
            'max_iterations': 100,   # stop earlier
            # include transitions that are possible, but not observed
            'feature.possible_transitions': True})
    else:
        trainer.set_params({
            'max_iterations': 100,   # stop earlier
            # include transitions that are possible, but not observed
            'feature.possible_transitions': True})


def _train(
        model_name: str,
        use_ULI2020: bool = False,
        use_sklearn: bool = False,
        max_count: int = 10000,
        optimize: bool = False,
        language_ids: Set[str] = None,
        algorithm: str = 'lbfgs'
):
    print('_train')
    x_train = []
    y_train = []
    if use_ULI2020:
        language_ids = _read_ULI2020(x_train, y_train, max_count)
    else:
        language_ids = _read_TED(x_train, y_train)
        language_ids.extend(_read_bibles(x_train, y_train))
    if use_sklearn:
        if optimize:
            rs = _optimize(
                x_train, y_train, list(language_ids), use_sklearn, algorithm)
            _check_parameter_space(rs)
            return
        crf = _make_crf(algorithm)
        crf.fit(x_train, y_train)
        dump(crf, model_name)
    else:
        if optimize:
            rs = _optimize(
                x_train, y_train, list(language_ids), algorithm=algorithm)
            return
        trainer = pycrfsuite.Trainer(algorithm=algorithm, verbose=False)
        for x, y in zip(x_train, y_train):
            trainer.append(x, y)
        _set_training(trainer, algorithm)
        trainer.train(model_name)
        print(trainer.logparser.last_iteration)
        if trainer.logparser.iterations:
            print(
                len(trainer.logparser.iterations),
                trainer.logparser.iterations[-1])


def _predict(
        model_name: str,
        file_name: str,
        use_sklearn: bool = False
):
    print('_predict')
    if use_sklearn:
        crf = load(model_name)
    else:
        crf = pycrfsuite.Tagger()
        crf.open(model_name)
    with open(file_name, encoding='utf-8') as file:
        text = file.read()
    print(' '.join(_sentence2tokens(text)), end='\n\n')
    if use_sklearn:
        print('Predicted:', ' '.join(crf.predict(_sentence2features(text))))
    else:
        print('Predicted:', ' '.join(crf.tag(_sentence2features(text))))


def _bio_classification_report(
        y_true, y_pred
):
    """
    Classification report for a list of BIO-encoded sequences.
    It computes token-level metrics and discards "O" labels.
    Note that it requires scikit-learn 0.15+ (or a version from github master)
    to calculate averages properly!
    """
    print('_bio_classification_report')
    lb = LabelBinarizer()
    y_true_combined = lb.fit_transform(list(chain.from_iterable(y_true)))
    y_pred_combined = lb.transform(list(chain.from_iterable(y_pred)))
    tagset = set(lb.classes_) - {'O'}
    tagset = sorted(tagset, key=lambda tag: tag.split('-', 1)[::-1])
    class_indices = {cls: idx for idx, cls in enumerate(lb.classes_)}
    return classification_report(
        y_true_combined,
        y_pred_combined,
        labels=[class_indices[cls] for cls in tagset],
        target_names=tagset,
    )


def _print_transitions(
        trans_features
):
    for (label_from, label_to), weight in trans_features:
        print('%-6s -> %-7s %0.6f' % (label_from, label_to, weight))


def _print_state_features(
        state_features
):
    for (attr, label), weight in state_features:
        print('%0.6f %-6s %s' % (weight, label, attr))


def _read_TED_test_sentences(
        language_ids: Set[str] = None,
        use_iso639_2: bool = False,
        test_count: int = 1000
) -> Tuple[List[str], List[str], Set[str]]:
    print('_read_TED_test_sentences')
    test_language_ids = set()
    language_codes = _read_TED_language_codes()
    x_test = []
    y_test = []
    lang_folder = 'TED-Multilingual-Parallel-Corpus/Monolingual_data'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if not isfile(lang_file_path) or not lang_file_name.endswith('.txt'):
            continue
        line_count = 0
        language_id = language_codes[lang_file_name.replace('.txt', '')]
        if use_iso639_2:
            language = languages.get(alpha_2=language_id)
            if not language:
                continue
            language_id = language.alpha_3
        if language_ids and language_id not in language_ids:
            continue
        test_language_ids.add(language_id)
        with open(lang_file_path, 'rt', encoding='utf-8') as lang_file:
            for line in lang_file:
                sentence_features = _sentence2features(line)
                if not sentence_features:
                    continue
                line_count += 1
                if test_count and line_count >= test_count:
                    break
                x_test.append(sentence_features)
                y_test.append(language_id)
    return x_test, y_test, test_language_ids


def _read_TED_code_switching_test_sentences(
        language_ids: Set[str] = None,
        use_iso639_2: bool = False,
        test_count: int = 10
) -> Tuple[List[str], List[str], Set[str]]:
    print('_read_TED_code_switching_test_sentences')
    test_language_ids = set()
    language_codes = _read_TED_language_codes()
    lang_index = 0
    line_index = 0
    x_test = []
    y_test = []
    lang_folder = 'TED-Multilingual-Parallel-Corpus/Monolingual_data'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if not isfile(lang_file_path) or not lang_file_name.endswith('.txt'):
            continue
        language_id = language_codes[lang_file_name.replace('.txt', '')]
        if use_iso639_2:
            language = languages.get(alpha_2=language_id)
            if not language:
                continue
            language_id = language.alpha_3
        if language_ids and language_id not in language_ids:
            continue
        test_language_ids.add(language_id)
        line_count = 0
        with open(lang_file_path, 'rt', encoding='utf-8') as lang_file:
            for line in lang_file:
                sentence_features = _sentence2features(line)
                if not sentence_features:
                    continue
                line_count += 1
                if test_count and line_count >= test_count:
                    break
                if line_index < len(x_test):
                    x_test[line_index] += sentence_features
                    y_test[line_index] += [language_id] * len(
                        sentence_features)
                else:
                    x_test += [sentence_features]
                    y_test += [[language_id] * len(sentence_features)]
                line_index += 1
        if line_count - max_count < test_count:
            continue
        lang_index += 1
        if lang_index % 2:
            line_index -= test_count
    return x_test, y_test, test_language_ids


def _read_ULI2020_test_sentences(
        max_count: int = 10000,
        test_count: int = 1000
) -> Tuple[List[str], List[str]]:
    print('_read_ULI2020_test_sentences')
    x_test = []
    y_test = []
    lang_folder = 'ULI2020_training'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if not isfile(lang_file_path) or not lang_file_name.endswith('.txt'):
            continue
        line_count = 0
        language_id = lang_file_name[:-len('.txt')].split('_')[0]
        if language_id.isupper():
            continue
        with open(lang_file_path, 'rt', encoding='utf-8') as lang_file:
            for line in lang_file:
                sentence_features = _sentence2features(line, use_ULI2020=True)
                if not sentence_features:
                    continue
                line_count += 1
                if line_count <= max_count:
                    continue
                if line_count >= max_count + test_count:
                    break
                x_test.append(sentence_features)
                y_test.append(language_id)
    return x_test, y_test


def _read_ULI2020_code_switching_test_sentences(
        max_count: int = 10000,
        test_count: int = 10
) -> Tuple[List[str], List[str]]:
    print('_read_ULI2020_code_switching_test_sentences')
    lang_index = 0
    line_index = 0
    x_test = []
    y_test = []
    lang_folder = 'ULI2020_training'
    for lang_file_name in listdir(lang_folder):
        lang_file_path = join(lang_folder, lang_file_name)
        if not isfile(lang_file_path) or not lang_file_name.endswith('.txt'):
            continue
        language_id = lang_file_name[:-len('.txt')].split('_')[0]
        if language_id.isupper():
            continue
        line_count = 0
        with open(lang_file_path, 'rt', encoding='utf-8') as lang_file:
            for line in lang_file:
                sentence_features = _sentence2features(line, use_ULI2020=True)
                if not sentence_features:
                    continue
                line_count += 1
                if line_count <= max_count:
                    continue
                if line_count >= max_count + test_count:
                    break
                if line_index < len(x_test):
                    x_test[line_index] += sentence_features
                    y_test[line_index] += [language_id] * len(
                        sentence_features)
                else:
                    x_test += [sentence_features]
                    y_test += [[language_id] * len(sentence_features)]
                line_index += 1
        if line_count - max_count < test_count:
            continue
        lang_index += 1
        if lang_index % 2:
            line_index -= test_count
    return x_test, y_test


def _test_ULI2020_task_sentences(
        crf: object,
        use_sklearn: bool = False
):
    print('_test_ULI2020_task_sentences')
    lang_folder = 'ULI2020_test'
    with open(join(
            lang_folder, 'test.txt'), 'rt', encoding='utf-8') as test_file:
        with open(
                'ULI-track-3-run-1-LangIdent.txt',
                'wt', encoding='utf-8') as submit_file:
            for line in test_file:
                x_seq = _sentence2features(line)
                if use_sklearn:
                    y_pred = crf.predict(x_seq)
                else:
                    y_pred = crf.tag(x_seq)
                submit_file.write(max(set(y_pred), key=y_pred.count) + '\n')


def _evaluate(
        model_name: str,
        use_ULI2020: bool = False,
        use_sklearn: bool = False
):
    print('_evaluate')
    if use_ULI2020:
        x_test, y_test = _read_ULI2020_test_sentences()
    else:
        x_test, y_test, _ = _read_TED_test_sentences()
    if use_sklearn:
        crf = load(model_name)
        y_pred = [crf.predict(x_seq) for x_seq in x_test]
        print('Top likely transitions:')
        _print_transitions(Counter(crf.transition_features_).most_common(20))
        print('\nTop unlikely transitions:')
        _print_transitions(Counter(
            crf.transition_features_).most_common()[-20:])
        print('Top positive:')
        _print_state_features(Counter(crf.state_features_).most_common(30))
        print('\nTop negative:')
        _print_state_features(Counter(crf.state_features_).most_common()[-30:])
    else:
        crf = pycrfsuite.Tagger()
        crf.open(model_name)
        y_pred = [crf.tag(x_seq) for x_seq in x_test]
        info = crf.info()
        print('Top likely transitions:')
        _print_transitions(Counter(info.transitions).most_common(15))
        print('\nTop unlikely transitions:')
        _print_transitions(Counter(info.transitions).most_common()[-15:])
        print('Top positive:')
        _print_state_features(Counter(info.state_features).most_common(20))
        print('\nTop negative:')
        _print_state_features(Counter(info.state_features).most_common()[-20:])
    print(_bio_classification_report(y_test, y_pred))


def _test(
        model_name: str,
        use_ULI2020: bool = False,
        use_only_bible_ids: bool = False,
        use_sklearn: bool = False,
        use_iso639_2: bool = False,
        max_count: int = 10000,
        test_count: int = 1000
):
    print('_test use_ULI2020 =', use_ULI2020)
    test_language_ids = None
    if use_ULI2020:
        language_ids = _read_ULI2020_language_ids()
        x_test, y_test = _read_ULI2020_test_sentences(max_count, test_count)
    else:
        if use_iso639_2:
            language_ids = _read_ULI2020_language_ids()
        else:
            language_ids = _read_bible_language_ids()
        x_test, y_test, test_language_ids = _read_TED_test_sentences(
            language_ids, use_iso639_2, test_count)
    if use_sklearn:
        crf = load(model_name)
    else:
        crf = pycrfsuite.Tagger()
        crf.open(model_name)
    count = 0
    predicted_count = 0
    predicted_language_max_count = 0
    y_preds = []
    y_tests = []
    predicted_language_ids = set()
    predicted_language_max_ids = set()
    for x_seq, y_label in zip(x_test, y_test):
        if not use_ULI2020 and use_only_bible_ids and \
                y_label not in language_ids:
            continue
        if use_sklearn:
            y_pred = crf.predict(x_seq)
        else:
            y_pred = crf.tag(x_seq)
        y_preds.append(y_pred)
        y_tests.append([y_label] * len(y_pred))
        predicted = all([y_pred1 == y_label for y_pred1 in y_pred])
        count += 1
        predicted_language_max = max(set(y_pred), key=y_pred.count) == y_label
        if predicted:
            predicted_count += 1
            predicted_language_ids.add(y_label)
        if predicted_language_max:
            predicted_language_max_count += 1
            predicted_language_max_ids.add(y_label)
    print('_test results use_ULI2020 =', use_ULI2020)
    if test_language_ids:
        print('test languages =', len(test_language_ids))
        language_ids = test_language_ids
    print('count =', count)
    print('predicted count =', predicted_count)
    print('percentage =', (predicted_count / count) * 100)
    print('predicted languages count =', len(predicted_language_ids))
    print('predicted language max count =', predicted_language_max_count)
    print('predicted language max percentage =', (
        predicted_language_max_count / count) * 100)
    print('predicted max languages count =', len(predicted_language_max_ids))
    score = flat_f1_score(
        y_tests, y_preds,
        average='weighted', labels=list(language_ids))
    print('flat_f1_score', score)
    sorted_labels = sorted(list(language_ids))
    print(flat_classification_report(
        y_tests, y_preds, labels=sorted_labels, digits=3))
    if use_ULI2020:
        _test_ULI2020_task_sentences(crf, use_sklearn)


def _test_code_switching(
        model_name: str,
        use_ULI2020: bool = False,
        use_sklearn: bool = False,
        use_iso639_2: bool = False,
        max_count: int = 10000,
        test_count: int = 10
):
    print('_test_code_switching use_ULI2020 =', use_ULI2020)
    test_language_ids = None
    if use_ULI2020:
        language_ids = _read_ULI2020_language_ids()
        x_test, y_test = _read_ULI2020_code_switching_test_sentences(
            max_count, test_count)
    else:
        if use_iso639_2:
            language_ids = _read_ULI2020_language_ids()
        else:
            language_ids = _read_bible_language_ids()
        x_test, y_test, test_language_ids = \
            _read_TED_code_switching_test_sentences(
                language_ids, use_iso639_2, test_count)
    if use_sklearn:
        crf = load(model_name)
    else:
        crf = pycrfsuite.Tagger()
        crf.open(model_name)
    count = 0
    predicted_count = 0
    language_set_predicted_count = 0
    y_preds = []
    predicted_language_ids = set()
    predicted_language_set_ids = set()
    for x_seq, y_seq in zip(x_test, y_test):
        if not x_seq:
            continue
        if use_sklearn:
            y_pred = crf.predict(x_seq)
        else:
            y_pred = crf.tag(x_seq)
        y_preds.append(y_pred)
        predicted = y_pred == y_seq
        language_set_predicted = set(y_pred) == set(y_seq)
        count += 1
        if predicted:
            predicted_count += 1
            predicted_language_ids.update(set(y_pred))
        if language_set_predicted:
            language_set_predicted_count += 1
            predicted_language_set_ids.update(set(y_pred))
    print('_test_code_switching results use_ULI2020 =', use_ULI2020)
    if test_language_ids:
        print('test languages =', len(test_language_ids))
        language_ids = test_language_ids
    print('count =', count)
    print('predicted count =', predicted_count)
    print('percentage =', (predicted_count / count) * 100)
    print('predicted language set count =', language_set_predicted_count)
    print('percentage language set =', (
        language_set_predicted_count / count) * 100)
    print('predicted languages count =', len(predicted_language_ids))
    print('predicted set languages count =', len(predicted_language_set_ids))
    score = flat_f1_score(
        y_test, y_preds,
        average='weighted', labels=list(language_ids))
    print('flat_f1_score', score)
    sorted_labels = sorted(list(language_ids))
    print(flat_classification_report(
        y_test, y_preds, labels=sorted_labels, digits=3))


plt.style.use('ggplot')
print('test/train/optimize/evaluate use_uli2020 max_count use_sklearn')
algorithm = 'l2sgd'
use_sklearn = len(argv) > 4
if len(argv) > 3:
    max_count = int(argv[3])
else:
    max_count = 10000
if len(argv) > 2:
    model_name = 'ULI2020.crf'
    use_ULI2020 = True
else:
    model_name = 'lang_ident.crf'
    use_ULI2020 = False
if len(argv) > 1:
    if argv[1] == 'train':
        _train(
            model_name, use_ULI2020, use_sklearn, max_count,
            algorithm=algorithm)
    elif argv[1] == 'optimize':
        _train(
            model_name, use_ULI2020, use_sklearn, max_count, optimize=True,
            algorithm=algorithm)
    elif argv[1] == 'evaluate':
        _evaluate(model_name, use_ULI2020)
    elif argv[1] == 'predict':
        _predict(model_name, file_name=argv[5], use_sklearn=use_sklearn)
        exit(0)
if use_ULI2020:
    _test_code_switching(
        model_name, use_sklearn=use_sklearn, use_iso639_2=True, max_count=0)
    _test(
        model_name, use_sklearn=use_sklearn, use_iso639_2=True, max_count=0)
_test_code_switching(
    model_name, use_ULI2020=use_ULI2020, use_sklearn=use_sklearn,
    max_count=max_count)
_test(
    model_name, use_ULI2020=use_ULI2020, use_only_bible_ids=True,
    use_sklearn=use_sklearn, max_count=max_count)
