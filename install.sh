#!/usr/bin/env bash
set -e

if [ -z "$2" ]; then
    pip install --upgrade joblib matplotlib numpy python-crfsuite pycountry 'scikit-learn<0.24' sklearn-crfsuite
fi
if [ ! -d 'TED-Multilingual-Parallel-Corpus' ]; then
    git clone https://github.com/ajinkyakulkarni14/TED-Multilingual-Parallel-Corpus.git
fi
if [ -z "$1" ]; then
    if [ ! -d 'bible-corpus' ]; then
    	git clone https://github.com/christos-c/bible-corpus.git
    fi
else
    echo 'ULI2020'
    if [ ! -f 'ULI2020_training.zip' ]; then
        wget http://suki.ling.helsinki.fi/ULI2020/ULI2020_training.zip
    fi
    if [ ! -f 'ULI2020_test.zip' ]; then
    	wget http://suki.ling.helsinki.fi/ULI2020/ULI2020_test.zip
    fi
    if [ ! -d 'ULI2020_training' ]; then
	    mkdir -p ULI2020_training
	    unzip ULI2020_training.zip -d ULI2020_training
        rm ULI2020_training.zip
    fi
    if [ ! -d 'ULI2020_test' ]; then
	    mkdir -p ULI2020_test
	    unzip ULI2020_test.zip -d ULI2020_test
        rm ULI2020_test.zip
    fi
fi
