Language Identification
====================

In Windows use Git CMD from
https://git-scm.com/download/win

Run
. env.sh
In Windows.

install.sh
installs needed python library, and clones two repository for Bible-training data.

install.sh u
reads data for ULI2020-training data

Trains and tests the model with Bible corpus:
python lang_ident.py train

with ULI2020 corpus:
python lang_ident.py train u


Evaluates the model:
python lang_ident.py evaluate


Tests the model with Bible model:
python lang_ident.py test

with ULI2020 model:
python lang_ident.py test u
