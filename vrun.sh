#!/usr/bin/env bash
set -e

source langident-env/bin/activate
WORK_DIR=/scratch/x/x
mkdir -p $WORK_DIR
pushd $WORK_DIR
python3 ~/langident/lang_ident.py train ULI2020
rm -f ~/ULI2020.zip
zip ~/ULI2020 ULI2020.crf
rm -f ~/ULI2020_test.zip
zip ~/ULI2020_test ULI-track-3-run-1-LangIdent.txt
popd
rm -f ~/submit_langident.zip
zip ~/submit_langident submit.out
