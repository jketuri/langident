#!/usr/bin/env bash
set -e

module load python-env
python3 --version
python3 -m venv --system-site-packages langident-env
source langident-env/bin/activate
pip3 install --upgrade pip
pip install --upgrade joblib matplotlib numpy python-crfsuite pycountry 'scikit-learn<0.24' sklearn-crfsuite
WORK_DIR=/scratch/x/x
mkdir -p $WORK_DIR
cd $WORK_DIR
~/langident/install.sh ULI2020 venv
